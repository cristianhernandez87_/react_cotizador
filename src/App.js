import React, { Fragment, useState } from 'react';
import Header from './components/Header';
import Form from './components/Form';
import Resumen from './components/Resumen';
import Result from './components/Result';

function App() {

  const [ resumen, saveResumen ] = useState({
    cotizacion: 0,
    datos: {
      brand: '',
      year: '',
      plan: ''
    }
  });

  // extrae datos
  const {cotizacion, datos } = resumen;

  return (
    <Fragment>
      <Header
        title="Cotizador Melosos"
        clase="container-fluid text-center mb-3"
      />

      <div className="container">
        <div className="row">
          <Form
            form_class="card col-12 col-md-7 mx-md-auto p-5 mb-3"
            form_g_class="mb-3 d-flex align-items-center"

            saveResumen={saveResumen}
          ></Form>

          <Resumen
            datos={datos}
          />

          <Result
            cotizacion={cotizacion}
          />
        </div>
      </div>
    </Fragment>
  );
}

export default App;
