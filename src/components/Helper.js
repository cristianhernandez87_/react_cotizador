
// obtiene diferencia de anos
export function getDirefenceYear(year) {
    return new Date().getFullYear() - year;
}

// calcula el totala pagar segun marca
export function calcBrand(brand) {
    let increase;

    switch(brand) {
        case 'european':
            increase = 1.30;
            break;
        case 'asian':
                increase = 1.05;
                break;
        case 'american':
            increase = 1.15;
            break;

        default:
            break;
    }
    return increase;
}

// calcular basico - completo
export function getPlan(plan) {
    return (plan === 'basic') ? 1.20 : 1.50;
}

// hacer mayusculas
export function mayus(texto) {
    return texto.charAt(0).toUpperCase() + texto.slice(1);
}