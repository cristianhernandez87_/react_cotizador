import React from 'react';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

const Result = ({cotizacion}) => {
    
    return (
        (cotizacion === 0)
            ?  <div className="col-12 col-md-7 mx-md-auto p-3 text-center">
                    <p>Agregue campos</p>
                </div>
            : (
                
                <div className="list-group-item-action active col-12 col-md-7 mx-md-auto p-3 text-center">
                    <TransitionGroup
                        component="p"
                        className="resultado"
                    >
                        <CSSTransition
                            classNames="resultado"
                            key={cotizacion}
                            timeout={{ enter:500, exit:500}}
                        >
                            <p>total de su seguro es: {cotizacion}</p>
                        </CSSTransition>
                    </TransitionGroup>
                </div>
                    
            )
    );
}

export default Result;